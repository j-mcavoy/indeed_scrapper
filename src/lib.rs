#![feature(try_trait)]

pub mod config;
pub mod doc_gen;
pub mod query_builder;
pub mod scraper;
