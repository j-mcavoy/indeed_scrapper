#![feature(async_closure)]
#![feature(try_trait)]

extern crate indeed_scraper;

use indeed_scraper::config;
use indeed_scraper::scraper::scrape;

#[tokio::main(core_threads = 16)]
async fn main() {
    let config: config::Config = config::parse_config("config.yml").expect("Config error");
    //    let query = IndeedQuery {
    //        city: City::CityState("San Francisco".into(), "CA".into()),
    //        radius: 15,
    //        max_age: 14,
    //        min_salary: 85004,
    //        experience_level: ExperienceLevel::EntryLevel,
    //        job_type: JobType::FullTime,
    //        contains_all_words: vec!["Software".into(), "Developer".into()],
    //        has_any_words: vec!["Unix".into(), "Embedded".in, "Rust", "Linux", "Full stack"],
    //        excludes_all_words: vec![".NET", "Azure", "Unpaid", "Senior", "Non-paid"],
    //        exclude_staffing_agencies: ExcludeStaffingAgencies::True,
    //        show_from: ShowFrom::Employer,
    //        limit: 50,
    //        ..IndeedQuery::default()
    //    };
    //

    let (queries, roles) = config.get_queries();
    for (i, query) in queries.iter().enumerate() {
        println!("Query {} of {}", i, queries.len());
        //println!("{:#?}", query);
        let role = roles[i].clone();
        println!("Role {:?}", role);
        if let Ok(res) = scrape(query.clone()).await {
            let _result = indeed_scraper::doc_gen::generate_cover_letter(
                std::path::Path::new(&config.get_project_dir()),
                res,
                role,
            );
        } else {
            println!("ERROR: {:?}", query);
        }
    }
}
