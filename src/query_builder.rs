//! # query
//!
//! Builds Indeed url query string from search params

use serde::{Deserialize, Serialize};
use std::fmt;
use url::{ParseError, Url};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub struct MaxAge(u32);
impl Default for MaxAge {
    fn default() -> Self {
        MaxAge(14)
    }
}
impl From<u32> for MaxAge {
    fn from(x: u32) -> Self {
        MaxAge(x)
    }
}

/// Sort descending by relevance to query or by date posted
#[derive(Clone, Debug, Copy, Serialize, Deserialize)]
pub enum SortBy {
    /// Job relevance to query
    Relevance,
    /// Date posted
    Date,
}
impl Default for SortBy {
    fn default() -> Self {
        Self::Relevance
    }
}
impl fmt::Display for SortBy {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use SortBy::*;
        write!(
            f,
            "{}",
            match self {
                Relevance => "relevance",
                Date => "date",
            },
        )
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub struct Limit(u32);
impl Default for Limit {
    fn default() -> Self {
        Self(50)
    }
}
impl From<u32> for Limit {
    fn from(x: u32) -> Self {
        Limit(x)
    }
}
/// Filter jobs by jobtite or employer
#[derive(Clone, Debug, Copy, Serialize, Deserialize)]
pub enum ShowFrom {
    All,
    JobSite,
    Employer,
}
impl Default for ShowFrom {
    fn default() -> Self {
        Self::All
    }
}
impl fmt::Display for ShowFrom {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ShowFrom::*;
        write!(
            f,
            "{}",
            match self {
                All => "",
                JobSite => "jobsite",
                Employer => "employer",
            },
        )
    }
}

/// Filter jobs by job type
#[derive(Clone, Debug, Copy, Serialize, Deserialize)]
pub enum JobType {
    AllJobTypes,
    FullTime,
    Contract,
    PartTime,
    Temporary,
    Internship,
    Commission,
}
impl Default for JobType {
    fn default() -> Self {
        Self::AllJobTypes
    }
}
impl fmt::Display for JobType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use JobType::*;
        write!(
            f,
            "{}",
            match self {
                AllJobTypes => "",
                FullTime => "fulltime",
                Contract => "contract",
                PartTime => "parttime",
                Temporary => "temporary",
                Internship => "internship",
                Commission => "commission",
            },
        )
    }
}

/// Filter jobs by required experience level
#[derive(Clone, Debug, Copy, Serialize, Deserialize)]
pub enum ExperienceLevel {
    /// Don't filter by experience level
    AllLevels,
    /// Entry Level jobs only
    EntryLevel,
    /// Mid Level jobs only
    MidLevel,
    /// Senoir Level jobs only
    SeniorLevel,
}
impl Default for ExperienceLevel {
    fn default() -> Self {
        Self::AllLevels
    }
}
impl fmt::Display for ExperienceLevel {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ExperienceLevel::*;
        write!(
            f,
            "{}",
            match self {
                AllLevels => "",
                EntryLevel => "entry_level",
                MidLevel => "mid_level",
                SeniorLevel => "senior_level",
            },
        )
    }
}

/// Whether to exclude staffing aggencies postings
#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum ExcludeStaffingAgencies {
    /// Exclude jobs posted by staffing agencies
    True,
    /// Don't exclude jobs posted by staffing agencies
    False,
}
impl Default for ExcludeStaffingAgencies {
    fn default() -> Self {
        Self::True
    }
}
impl fmt::Display for ExcludeStaffingAgencies {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ExcludeStaffingAgencies::*;
        write!(
            f,
            "{}",
            match self {
                True => "directhire",
                False => "",
            },
        )
    }
}

/// Filter by city
#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum City {
    /// `CityState` is the city and state: (City, State)
    CityState(String, String),
    /// ``ZipCode` is the zip code of the city
    ZipCode(String),
}
impl Default for City {
    fn default() -> Self {
        Self::ZipCode("".to_string())
    }
}
impl fmt::Display for City {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use City::*;
        write!(
            f,
            "{}",
            match self {
                CityState(city, state) => format!("{}, {}", city, state),
                ZipCode(zip) => zip.to_string(),
            },
        )
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct IndeedQuery {
    /// target city
    pub city: City,
    /// maximum distance in miles from target city
    pub radius: u32,
    /// max number of days since jobs were posted
    pub max_age: MaxAge,
    /// minimum expected salary for all jobs
    pub min_salary: u32,
    /// experience level filter for all jobs
    pub experience_level: ExperienceLevel,
    /// job type filter for all jobs
    pub job_type: JobType,
    /// job posting must contain all keywords
    pub contains_all_words: Vec<String>,
    /// job posting must contain exact phrase
    pub has_exact_phrase: String,
    /// job posting must contain at least one keyword
    pub has_any_words: Vec<String>,
    /// job posting must not contain any keyword
    pub excludes_all_words: Vec<String>,
    /// job posting must contain all words in job title
    pub has_words_in_title: Vec<String>,
    /// jobs only posted by a specified company
    pub company: String,
    /// whether or not to include postings from staffing agencies in result
    pub exclude_staffing_agencies: ExcludeStaffingAgencies,
    /// show from filter
    pub show_from: ShowFrom,
    /// sort resuts by relevance or date posted
    pub sort_by: SortBy,
    /// limit number of jobs per page
    pub limit: Limit,
    /// show results starting after `n` number of jobs
    pub start: u32,
}
impl Default for IndeedQuery {
    fn default() -> Self {
        Self {
            city: City::default(),
            radius: 30,
            max_age: 14.into(),
            min_salary: 85000,
            experience_level: ExperienceLevel::default(),
            job_type: JobType::default(),
            contains_all_words: vec![],
            has_exact_phrase: String::default(),
            has_any_words: vec![],
            excludes_all_words: vec![],
            has_words_in_title: vec![],
            company: String::default(),
            exclude_staffing_agencies: ExcludeStaffingAgencies::default(),
            show_from: ShowFrom::default(),
            sort_by: SortBy::default(),
            limit: Limit::default(),
            start: 0,
        }
    }
}
impl IndeedQuery {
    /// convert query to url string
    pub fn build_url(&self) -> Result<String, ParseError> {
        let url = Url::parse_with_params(
            "https://www.indeed.com/jobs",
            &[
                ("as_and", self.contains_all_words.join(" ")),
                ("as_phr", self.has_exact_phrase.to_string()),
                ("as_any", self.has_any_words.join(" ")),
                ("as_not", self.excludes_all_words.join(" ")),
                ("as_ttl", self.has_words_in_title.join(" ")),
                ("as_cmp", self.company.to_string()),
                ("jt", self.job_type.to_string()),
                ("st", self.show_from.to_string()),
                ("explvl", self.experience_level.to_string()),
                ("sr", self.exclude_staffing_agencies.to_string()),
                ("salary", self.min_salary.to_string()),
                ("radius", self.radius.to_string()),
                ("l", self.city.to_string()),
                ("fromage", self.max_age.0.to_string()),
                ("limit", self.limit.0.to_string()),
                ("sort", self.sort_by.to_string()),
                ("psf", "advsrch".to_string()),
                ("from", "advancedsearch".to_string()),
                ("start", self.start.to_string()),
            ],
        )?;
        Ok(url.to_string())
    }

    /// increments start offset to next page of results
    pub fn increment_page(&mut self) {
        self.start += self.limit.0;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn build_url_test() {
        let query = IndeedQuery {
            city: City::CityState("San Francisco", "CA"),
            radius: 15,
            max_age: 14,
            min_salary: 85004,
            experience_level: ExperienceLevel::EntryLevel,
            job_type: JobType::FullTime,
            contains_all_words: vec!["Software", "Developer"],
            has_any_words: vec!["Unix", "Embedded", "Rust", "Linux", "Full stack"],
            excludes_all_words: vec![".NET", "Azure", "Unpaid", "Senior", "Non-paid"],
            exclude_staffing_agencies: ExcludeStaffingAgencies::True,
            show_from: ShowFrom::Employer,
            ..IndeedQuery::default()
        };
        assert_eq!(query.build_url().unwrap(), "https://www.indeed.com/jobs?as_and=&as_phr=&as_any=Embedded+Full-Stack+Linux+Rust+Unix+Web&as_not=C%23+.NET+Azure+Unpaid+Senior&as_ttl=Developer&as_cmp=&jt=fulltime&st=&explvl=entry_level&sr=directhire&salary=85000&radius=40&l=San+Francisco%2C+CA&fromage=14&limit=50&sort=date&psf=advsrch&from=advancedsearch&start=0");
    }
}
