use super::scraper::*;
use chrono::Utc;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fs::*;
use std::io::Write;
use std::path::Path;

pub type GenResult = Result<(), GenError>;

#[derive(Deserialize, Serialize, Clone, Debug)]
pub enum Role {
    FullStackWebDeveloper,
    HardwareSoftwareGeneralist,
    RustDeveloper,
}
impl Default for Role {
    fn default() -> Self {
        Self::FullStackWebDeveloper
    }
}
impl fmt::Display for Role {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::FullStackWebDeveloper => write!(f, "full_stack_web_developer"),
            Self::HardwareSoftwareGeneralist => write!(f, "hard_software_generalist"),
            Self::RustDeveloper => write!(f, "rust_developer"),
        }
    }
}

pub enum GenError {
    GenError,
}
impl From<std::io::Error> for GenError {
    fn from(_: std::io::Error) -> Self {
        Self::GenError
    }
}

pub fn generate_cover_letter(base_dir: &Path, map: JobMap, role: Role) -> GenResult {
    println!("Generating docs");
    let company_template = read_to_string(base_dir.join("templates").join("company.tex"))?;
    let job_template = read_to_string(base_dir.join("templates").join("job.tex"))?;

    for (company, jobs) in map {
        let company_dir = base_dir.join("companies").join(
            &company
                .name
                .to_lowercase()
                .replace("/", "")
                .replace("\\", "")
                .replace("(", "")
                .replace(")", "")
                .replace(" ", "_")
                .replace(":", "")
                .replace(",", ""),
        );

        let jobs_dir = company_dir.join("jobs");
        println!("{:?}", jobs_dir);
        create_dir_all(&jobs_dir).expect("Jobs dir err");

        let company_path = company_dir.join("company.tex");

        if !company_path.exists() {
            let mut company_file: File = File::create(company_path).expect("Company file Err");
            company_file.write_all(
                company_template
                    .replace("COMPANYNAME", &company.name)
                    .replace("COMPANYHOMEPAGE", &company.homepage.unwrap_or_default())
                    .replace("ADDRESS", &company.location)
                    .as_bytes(),
            )?;
        }

        for job in jobs {
            let job_path = company_dir.join("jobs").join(
                [
                    &job.job_title
                        .to_lowercase()
                        .replace("/", "")
                        .replace("\\", "")
                        .replace("(", "")
                        .replace(")", "")
                        .replace(" ", "_")
                        .replace(":", "")
                        .replace(",", ""),
                    ".tex",
                ]
                .join(""),
            );
            if !job_path.exists() {
                println!("Job file: {:?}", job_path);
                let mut job_tex: File = File::create(&job_path).expect("Job tex error");
                let mut jd = vec![];
                for line in job.job_description.unwrap_or_default().split("\n") {
                    if line.len() > 2 {
                        jd.push(["% ", line].join(""));
                    }
                }
                let now = &Utc::now().to_rfc3339();

                job_tex.write_all(
                    job_template
                        .replace("SCRAPED_DATE", now)
                        .replace("DID_APPLY", "false")
                        .replace("JOBURL", &job.url)
                        .replace("JOBTITLE", &job.job_title)
                        .replace("JOBLOCATION", &job.location)
                        .replace("JOB_SUMMARY", &jd.join("\n"))
                        .replace("EASYAPPLY", &job.is_easy_apply.to_string())
                        .replace("ROLE", &role.to_string())
                        .as_bytes(),
                )?;
            } else {
                println!("WARNING Job {:?} exists", job_path);
            }
        }
    }
    println!("Docs finished");
    Ok(())
}
