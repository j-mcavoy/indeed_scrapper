use super::doc_gen::Role;
use super::query_builder::*;
use serde::{Deserialize, Serialize};
use std::fs;

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Config {
    global: MultiQuery,
    queries: Vec<MultiQuery>,
    project_dir: String,
}
impl Config {
    pub fn get_project_dir(&self) -> String {
        self.project_dir.clone()
    }

    pub fn get_queries(&self) -> (Vec<IndeedQuery>, Vec<Role>) {
        let mut out: Vec<IndeedQuery> = vec![];
        let mut roles: Vec<Role> = vec![];
        let queries: &Vec<MultiQuery> = &self.queries;

        let global = &self.global;

        for q in queries {
            for cr in &q
                .cities
                .clone()
                .into_iter()
                .chain(global.cities.clone().into_iter())
                .collect::<Vec<CityRadius>>()
            {
                for exp in &q
                    .experience_levels
                    .clone()
                    .into_iter()
                    .chain(global.experience_levels.clone().into_iter())
                    .collect::<Vec<ExperienceLevel>>()
                {
                    for jt in &q
                        .job_types
                        .clone()
                        .into_iter()
                        .chain(global.job_types.clone().into_iter())
                        .collect::<Vec<JobType>>()
                    {
                        roles.push(q.role.clone());
                        IndeedQuery::default();
                        out.push(IndeedQuery {
                            city: cr.city.clone(),
                            radius: cr.radius,
                            company: q.company.clone(),
                            max_age: global.max_age.into(),
                            min_salary: global.min_salary,
                            contains_all_words: q
                                .contains_all_words
                                .clone()
                                .into_iter()
                                .chain(global.contains_all_words.clone().into_iter())
                                .collect(),
                            has_any_words: q
                                .has_any_words
                                .clone()
                                .into_iter()
                                .chain(global.has_any_words.clone().into_iter())
                                .collect(),
                            has_words_in_title: q
                                .has_words_in_title
                                .clone()
                                .into_iter()
                                .chain(global.has_any_words.clone().into_iter())
                                .collect(),
                            has_exact_phrase: q.has_exact_phrase.clone(),
                            excludes_all_words: q
                                .excludes_all_words
                                .clone()
                                .into_iter()
                                .chain(global.excludes_all_words.clone().into_iter())
                                .collect(),
                            show_from: q.show_from,
                            sort_by: q.sort_by,
                            job_type: *jt,
                            experience_level: *exp,
                            limit: q.limit,
                            ..IndeedQuery::default()
                        });
                    }
                }
            }
        }
        (out, roles)
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
struct CityRadius {
    city: City,
    radius: u32,
}

#[derive(Serialize, Deserialize, Debug, Default)]
struct MultiQuery {
    #[serde(default)]
    cities: Vec<CityRadius>,
    #[serde(default)]
    max_age: MaxAge,
    #[serde(default)]
    min_salary: u32,
    #[serde(default)]
    experience_levels: Vec<ExperienceLevel>,
    #[serde(default)]
    job_types: Vec<JobType>,
    #[serde(default)]
    has_any_words: Vec<String>,
    #[serde(default)]
    contains_all_words: Vec<String>,
    #[serde(default)]
    has_exact_phrase: String,
    #[serde(default)]
    excludes_all_words: Vec<String>,
    #[serde(default)]
    has_words_in_title: Vec<String>,
    #[serde(default)]
    company: String,
    #[serde(default)]
    show_from: ShowFrom,
    #[serde(default)]
    sort_by: SortBy,
    #[serde(default)]
    limit: Limit,
    #[serde(default)]
    role: Role,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum ConfigError {
    FileNotFound,
    ParseError,
}

impl From<std::io::Error> for ConfigError {
    fn from(_err: std::io::Error) -> Self {
        Self::FileNotFound
    }
}
impl From<serde_yaml::Error> for ConfigError {
    fn from(_: serde_yaml::Error) -> Self {
        Self::ParseError
    }
}

pub fn parse_config(path: &str) -> Result<Config, ConfigError> {
    let s = fs::read_to_string(path).unwrap();
    let cfg: Config = serde_yaml::from_str(&s).unwrap();
    Ok(cfg)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_config() {
        let _cfg = parse_config("config.yml").unwrap();
    }
}
