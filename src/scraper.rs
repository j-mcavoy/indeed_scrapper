use super::query_builder::*;
use reqwest::Client;
use scraper::{Html, Selector};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use url::Url;

pub type JobMap = HashMap<Company, Vec<Job>>;
pub type CompanyMap = HashMap<String, Company>;
pub type ScrapeResult = std::result::Result<JobMap, ScrapeError>;

#[derive(Debug, Clone, PartialEq, Hash, Eq, Default, Serialize, Deserialize)]
pub struct Company {
    pub name: String,
    pub location: String,
    pub homepage: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Hash, Eq, Default, Serialize, Deserialize)]
pub struct Job {
    pub job_title: String,
    pub location: String,
    pub job_description: Option<String>,
    pub url: String,
    pub is_easy_apply: bool,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum Salary {
    AnnualFixed(u32),
    AnnualRange(u32, u32),
    HourlyFixed(f32),
    HourlyRange(f32),
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum ScrapeError {
    CantFindHost,
    InvalidUrl,
    ParsingFailure,
    RequestFailure,
    NoElementFound,
}
impl From<std::num::ParseIntError> for ScrapeError {
    fn from(_err: std::num::ParseIntError) -> Self {
        Self::ParsingFailure
    }
}
impl From<reqwest::Error> for ScrapeError {
    fn from(err: reqwest::Error) -> Self {
        if err.is_timeout() {
            Self::CantFindHost
        } else if err.is_decode() {
            Self::ParsingFailure
        } else {
            Self::RequestFailure
        }
    }
}
impl From<std::option::NoneError> for ScrapeError {
    fn from(_err: std::option::NoneError) -> Self {
        ScrapeError::NoElementFound
    }
}
impl From<url::ParseError> for ScrapeError {
    fn from(_err: url::ParseError) -> Self {
        ScrapeError::InvalidUrl
    }
}

pub async fn scrape(query: IndeedQuery) -> ScrapeResult {
    let mut q = query.to_owned();
    let client = reqwest::Client::new();
    let url = query.build_url()?;
    println!("{}", url);
    let body = &client.get(&url).send().await?.text().await?;
    let html = Html::parse_document(&body);
    let job_count = parse_job_count(&html).await?;

    let mut urls = vec![];
    while q.start < job_count {
        println!("{} of {}", q.start, job_count);
        urls.push(q.build_url()?);
        q.increment_page();
    }

    let mut companies = CompanyMap::new();
    let mut map = JobMap::new();

    for url in urls {
        println!("Running task search_page: {}", url);
        let res = &client.get(&url).send().await?.text().await?;
        let page = Html::parse_document(&res);
        let jobs = parse_jobs(page, &mut companies, &client).await;
        if let Ok(jobs) = jobs {
            map.extend(jobs);
        }
    }

    Ok(map)
}

async fn parse_job_count(html: &Html) -> Result<u32, ScrapeError> {
    let selector = Selector::parse("#searchCountPages").unwrap();
    let el = html.select(&selector).next()?.inner_html().replace(",", "");
    let split: Vec<&str> = el.split(" ").collect();
    let job_count = split[split.len() - 2].parse::<u32>()?;
    println!("Job Count {}", job_count);
    Ok(job_count)
}

async fn parse_jobs(
    search_page: Html,
    companies: &mut CompanyMap,
    client: &Client,
) -> ScrapeResult {
    let mut map = JobMap::new();
    let selector = Selector::parse(".jobsearch-SerpJobCard").unwrap();
    for job_el in search_page.select(&selector) {
        let frag: &Html = &Html::parse_fragment(&job_el.html());
        let company = parse_company(&frag, companies, &client).await;
        if let Ok(company) = company {
            let job = parse_job(&frag, &client);
            if map.contains_key(&company) {
                map.get_mut(&company)
                    .expect("Could not find company")
                    .push(job.await?);
            } else {
                map.insert(company, vec![job.await?]);
            }
        }
    }
    println!("All Jobs on page Parsed!");
    Ok(map)
}

async fn parse_job(html: &Html, client: &Client) -> Result<Job, ScrapeError> {
    let mut job = Job::default();

    let base_url: Url = Url::parse("https://www.indeed.com").unwrap();
    let job_title_sel = Selector::parse(".jobtitle").unwrap();
    let el = html.select(&job_title_sel).next()?;
    let href = el.value().attr("href")?;

    let url = base_url.join(&href)?;
    let job_detail_page = client.get(url.as_str()).send().await?.text().await?;
    let p = Html::parse_document(&job_detail_page);
    let job_description = parse_job_description(&p);

    let job_title = el.value().attr("title")?.to_string();
    let location_sel = Selector::parse(".location").unwrap();
    let location = html
        .select(&location_sel)
        .next()?
        .text()
        .collect::<Vec<_>>()[0]
        .to_string();
    let is_easy_apply_sel = Selector::parse(".indeedApply").unwrap();
    let is_easy_apply = html.select(&is_easy_apply_sel).next().is_some();

    job = Job {
        job_description: job_description.await,
        url: url.to_string(),
        job_title,
        location,
        is_easy_apply,
        ..job
    };

    println!("Parsed Job: {}", job.job_title);
    Ok(job)
}

async fn parse_job_description(html: &Html) -> Option<String> {
    let selector = Selector::parse("#jobDescriptionText").unwrap();
    let el = html.select(&selector).next();
    match el {
        Some(e) => Some(
            e.text()
                .filter(|l| l.len() > 2)
                .collect::<Vec<&str>>()
                .join("\n"),
        ),
        None => None,
    }
}

async fn parse_company(
    job_html: &Html,
    map: &mut CompanyMap,
    client: &Client,
) -> Result<Company, ScrapeError> {
    let company_page_sel = Selector::parse(".company").unwrap();
    let company_name_el = job_html.select(&company_page_sel).next()?;

    let name = company_name_el
        .text()
        .collect::<Vec<&str>>()
        .join("")
        .to_string()
        .replace("\n", "");

    match map.get_mut(&name) {
        Some(c) => Ok(c.clone()),
        None => {
            let location_sel = Selector::parse(".location").unwrap();
            let location: String = job_html
                .select(&location_sel)
                .next()?
                .inner_html()
                .to_string();
            let mut company = Company {
                name: name.clone(),
                location,
                ..Company::default()
            };

            let detail_sel = Selector::parse("a").unwrap();
            let company_detail_url = company_name_el.select(&detail_sel).next();
            if let Some(cd) = company_detail_url {
                let mut href: String = cd.value().attr("href").unwrap().to_owned();
                href.push('/');
                let base_url: Url = Url::parse("https://www.indeed.com").unwrap();

                if let Ok(url) = base_url.join(&href) {
                    let url = url.join("about/")?;
                    let res = client.get(url.as_str()).send().await?.text().await?;
                    let link_sel =
                        Selector::parse(".cmp-AboutBasicCompanyDetailsWidget-companyLink").unwrap();
                    if let Some(homepage_el) = Html::parse_document(&res).select(&link_sel).last() {
                        let homepage = homepage_el.value().attr("href")?.to_string();
                        company = Company {
                            homepage: Some(homepage),
                            ..company
                        };
                    }
                }
            }

            println!("Parsed company: {}", company.name);
            map.insert(name, company.clone());
            Ok(company)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() {
        let query = IndeedQuery {
            city: City::CityState("San Francisco", "CA"),
            radius: 15,
            max_age: 14,
            min_salary: 85004,
            experience_level: ExperienceLevel::EntryLevel,
            job_type: JobType::FullTime,
            contains_all_words: vec!["Software", "Developer"],
            has_any_words: vec!["Unix", "Embedded", "Rust", "Linux", "Full stack"],
            excludes_all_words: vec![".NET", "Azure", "Unpaid", "Senior", "Non-paid"],
            exclude_staffing_agencies: ExcludeStaffingAgencies::True,
            show_from: ShowFrom::Employer,
            ..IndeedQuery::default()
        };
        assert_eq!(query.build_url().unwrap(), "https://www.indeed.com/jobs?as_and=&as_phr=&as_any=Embedded+Full-Stack+Linux+Rust+Unix+Web&as_not=C%23+.NET+Azure+Unpaid+Senior&as_ttl=Developer&as_cmp=&jt=fulltime&st=&explvl=entry_level&sr=directhire&salary=85000&radius=40&l=San+Francisco%2C+CA&fromage=14&limit=50&sort=date&psf=advsrch&from=advancedsearch&start=0");
    }
}
